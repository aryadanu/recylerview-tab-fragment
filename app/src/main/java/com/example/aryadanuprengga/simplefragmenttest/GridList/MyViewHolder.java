package com.example.aryadanuprengga.simplefragmenttest.GridList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aryadanuprengga.simplefragmenttest.R;

/**
 * Created by Arya Danu Prengga on 6/12/2017.
 */

public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView img;
    TextView img_text;
    itemsClickListener itemsClickListener;

    public MyViewHolder(View itemView){
        super(itemView);

        img=(ImageView) itemView.findViewById(R.id.imagefilm);
        img_text=(TextView) itemView.findViewById(R.id.img_text);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(itemsClickListener ic){
        this.itemsClickListener=ic;

    }
    @Override
    public void onClick(View v){
        this.itemsClickListener.onItemClick(v,getLayoutPosition());
    }
}
