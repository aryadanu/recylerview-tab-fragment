package com.example.aryadanuprengga.simplefragmenttest.Recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.aryadanuprengga.simplefragmenttest.R;

/**
 * Created by Arya Danu Prengga on 6/12/2017.
 */

public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView img;
    TextView nameTxt,text_rating;
    RatingBar ratingBar;

    itemsClickListener itemsClickListener;
    public MyViewHolder(View itemView){
        super(itemView);

        nameTxt=(TextView) itemView.findViewById(R.id.nama_chat);
        img=(ImageView) itemView.findViewById(R.id.profil);
        ratingBar=(RatingBar) itemView.findViewById(R.id.rating);
        text_rating=(TextView) itemView.findViewById(R.id.text_rating);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(itemsClickListener ic){
        this.itemsClickListener=ic;

    }
    @Override
    public void onClick(View v){
        this.itemsClickListener.onItemClick(v,getLayoutPosition());
    }
}
