package com.example.aryadanuprengga.simplefragmenttest.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.aryadanuprengga.simplefragmenttest.R;
import com.example.aryadanuprengga.simplefragmenttest.Recyclerview.FragmentAdapter;
import com.example.aryadanuprengga.simplefragmenttest.Recyclerview.NowPlaying;

import java.util.ArrayList;


public class Fragment1 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment1, null);

        //RecyclerView
        RecyclerView rv = (RecyclerView) v.findViewById(R.id.myRecylerviewFrag1);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rv.setAdapter(new FragmentAdapter(this.getActivity(), getNowPlaying()));
        return v;

    }

    private ArrayList<NowPlaying> getNowPlaying() {


      //COLLECTION ARRAY NOW PLAYING
        ArrayList<NowPlaying>nowPlayings=new ArrayList<>();
        // SINGLE NOW PLAYING
        NowPlaying nowPlaying=new NowPlaying("Spider-Man : Homecoming","4.0",4,R.drawable.spiderman);
        //ADD TO COLLECTION
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Avengers : Infinity Wars","5.0",5,R.drawable.avengers);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Inhumans","3.0",3,R.drawable.inhumans);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Justice League","4.0",4,R.drawable.justiceleage);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 5","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 6","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 7","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 8","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 9","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);

        nowPlaying=new NowPlaying("Spider-Man : Homecoming 10","4.0",4,R.drawable.spiderman);
        nowPlayings.add(nowPlaying);


    return  nowPlayings;
    }

}