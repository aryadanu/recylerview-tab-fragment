package com.example.aryadanuprengga.simplefragmenttest.Recyclerview;

/**
 * Created by Arya Danu Prengga on 6/12/2017.
 */

public class NowPlaying {

    private String  name,text_rating;
    private float rating;
    private  int images;

    public NowPlaying(String name, String text_rating, float rating, int images) {
        this.name = name;
        this.images = images;
        this.rating=rating;
        this.text_rating=text_rating;
    }

    public String getText_rating() {
        return text_rating;
    }

    public void setText_rating(String text_rating) {
        this.text_rating = text_rating;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
