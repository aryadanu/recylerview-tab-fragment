package com.example.aryadanuprengga.simplefragmenttest.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.aryadanuprengga.simplefragmenttest.GridList.ComingSoon;
import com.example.aryadanuprengga.simplefragmenttest.GridList.FragmentAdapter;
import com.example.aryadanuprengga.simplefragmenttest.R;


import java.util.ArrayList;

public class Fragment2 extends Fragment {
//    ViewPager viewPager1;
//    ViewPagerAdapter vPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment2,null);

        //RecyclerView
        RecyclerView rv = (RecyclerView) v.findViewById(R.id.myRecylerviewFrag2);
        rv.setLayoutManager(new GridLayoutManager(this.getActivity(),3));
        rv.setAdapter(new FragmentAdapter(this.getActivity(), getComingSoon()));
        //Disable Scrool in RecyclerView(GridVIew)
        rv.setNestedScrollingEnabled(false);
        return v;

    }
    private ArrayList<ComingSoon> getComingSoon() {
        //COLLECTION ARRAY HOTEL
        ArrayList<ComingSoon>comingSoons=new ArrayList<>();
        // SINGLE HOTEL
        ComingSoon film=new ComingSoon(R.drawable.spiderman,"Spiderman Home Coming");
        //ADD TO COLLECTION
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.despicable,"Despicable Me 3");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.transformer,"Transformer The Last Knight");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.inhumans,"INHUMANS");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.cars3,"CARS 3");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.thor,"Thor : Ragnarok");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.justiceleage,"Justice League");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.avengers,"Avengers The Infinity War:Part 1");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.wonderwoman,"Wonder Woman");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.starward,"StarWars : The Last Jedi");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.guardianofgalaxy,"Guardian Of Galaxy 2");
        comingSoons.add(film);
        film=new ComingSoon(R.drawable.logan,"Logan");
        comingSoons.add(film);

        return  comingSoons;

    }

}
