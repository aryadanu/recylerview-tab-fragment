package com.example.aryadanuprengga.simplefragmenttest.GridList;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.aryadanuprengga.simplefragmenttest.R;

import java.util.ArrayList;

/**
 * Created by Arya Danu Prengga on 6/11/2017.
 */

public class FragmentAdapter extends RecyclerView.Adapter<MyViewHolder> {

    Context c;
    ArrayList<ComingSoon> comingSoons;

    public FragmentAdapter(Context c, ArrayList<ComingSoon> comingSoons) {
        this.c = c;
        this.comingSoons = comingSoons;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_layout,null);
        MyViewHolder holder=new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.img.setImageResource(comingSoons.get(position).getImages());
        holder.img_text.setText(comingSoons.get(position).getText());
//        holder.setItemClickListener(new itemsClickListener() {
//            @Override
//            public void onItemClick(View v, int pos) {
//                Intent i= new Intent(c,Details.class);
//                c.startActivity(i);
//            }
//        });

        //Listener
    }

    @Override
    public int getItemCount() {
        return comingSoons.size();
    }
}
