package com.example.aryadanuprengga.simplefragmenttest.GridList;

/**
 * Created by Arya Danu Prengga on 6/13/2017.
 */

public class ComingSoon {

    private int images;
    private String text;


    public ComingSoon(int images,String text) {

        this.images = images;
        this.text=text;

    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }
}


